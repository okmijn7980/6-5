﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoProject.Models.partical
{   
    [MetadataType(typeof(NewsMetadata))]
    public class NewsPartial

    {
    }

    public class NewsMetadata
    {
        [DisplayName("標題")]
        [Required]
        [StringLength(50)]
        public string Tittle { get; set; }

        [DisplayName("內容")]
        [Required]
        [StringLength(500)]
        public string Content { get; set; }

        [DisplayName("建立時間")]
        [Required]
        public System.DateTime CreateTime { get; set; }
    } 

}